<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => 'Молоко',
            'category_of_products_id' => 1,
            'price' => 75,
            'quantity' => 20,
            'body' => 'Молоко от фикрмы Белая Река'
        ]);

        DB::table('products')->insert([
            'name' => 'J7 Апельсиновый',
            'category_of_products_id' => 2,
            'price' => 100,
            'quantity' => 15,
            'body' => 'Очень вкусный апельсиновый сок'
        ]);

        DB::table('products')->insert([
            'name' => 'Баранина',
            'category_of_products_id' => 3,
            'price' => 75,
            'quantity' => 20,
            'body' => 'Свежее и экологически чистое мясо'
        ]);

        DB::table('products')->insert([
            'name' => 'Огурцы',
            'category_of_products_id' => 4,
            'price' => 50,
            'quantity' => 30,
            'body' => 'Гладкие огурцы'
        ]);

        DB::table('products')->insert([
            'name' => 'Бананы',
            'category_of_products_id' => 5,
            'price' => 120,
            'quantity' => 15,
            'body' => 'Российские бананы'
        ]);

        Product::factory(30)->create();
    }
}
