<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoryOfProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category_of_products')->insert([
            'name' => 'Молочная продукция'
        ]);

        DB::table('category_of_products')->insert([
            'name' => 'Сок'
        ]);

        DB::table('category_of_products')->insert([
            'name' => 'Мясо'
        ]);

        DB::table('category_of_products')->insert([
            'name' => 'Овощи'
        ]);

        DB::table('category_of_products')->insert([
            'name' => 'Фрукты'
        ]);
    }
}
