<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
            'category_of_products_id' => rand(1, 5),
            'price' => rand(50, 1000),
            'quantity' => rand(10, 50),
            'body' => $this->faker->text(rand(50, 150))
        ];
    }
}
