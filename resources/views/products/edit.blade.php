@extends('layouts.app')

@section('content')

    <h2>Редактирование продукта</h2>

    <form method="post" action="{{route('products.update', ['product' => $product])}}">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="name">Название</label>
            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{$product->name}}">
            @error('name')
            <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>

        <div class="form-group">
            <label for="category_of_products_id">Категория</label>
            <select multiple class="form-control" id="category_of_products_id" name="category_of_products_id">
                @foreach($categories_of_products as $category_of_products)
                    <option value="{{$category_of_products->id}}"
                            @if($product->category_of_products->id == $category_of_products->id) selected @endif>
                        {{$category_of_products->name}}
                    </option>
                @endforeach
            </select>

            @error('category_of_products_id')
            <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>

        <div class="form-group">
            <label for="price">Цена</label>
            <input type="text" class="form-control @error('price') is-invalid @enderror" id="price" name="price" value="{{$product->price}}">
            @error('price')
            <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>

        <div class="form-group">
            <label for="quantity">Количество</label>
            <input type="text" class="form-control @error('quantity') is-invalid @enderror" id="quantity" name="quantity" value="{{$product->quantity}}">
            @error('quantity')
            <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>

        <div class="form-group">
            <label for="body">Описание</label>
            <textarea class="form-control @error('body') is-invalid @enderror" id="body" name="body" rows="3">{{$product->body}}</textarea>
            @error('body')
            <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>

        <button type="submit" class="btn btn-success">Редактировать</button>

    </form>

    <div class="mt-3 mb-3">
        <a href="{{route('index')}}">Назад</a>
    </div>

@endsection
