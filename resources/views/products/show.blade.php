@extends('layouts.app')

@section('content')

    <div class="card" style="width: 18rem;">
        <div class="card-body">
            <h5 class="card-title">{{$product->name}}</h5>
            <h6 class="card-subtitle mb-2 text-muted">{{$product->category_of_products->name}}</h6>
            <h6 class="card-subtitle mb-2 text-muted">Цена: {{$product->price}}</h6>
            <h6 class="card-subtitle mb-2 text-muted">Количество: {{$product->quantity}}</h6>
            <p class="card-text">{{$product->body}}</p>
            @can('viewAny', Auth::User())
            <a href="{{route('products.edit', ['product' => $product])}}" class="card-link">Редактировать</a>

            <form class="card-link" method="post" action="{{route('products.destroy', ['product' => $product])}}">
                @method('delete')
                @csrf
                <button type="submit" class="btn btn-outline-danger card-link">Удалить</button>
            </form>
            @endcan
        </div>
    </div>

    <div class="mt-3 mb-3">
        <a href="{{route('index')}}">Назад</a>
    </div>

@endsection
