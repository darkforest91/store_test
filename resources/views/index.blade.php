@extends('layouts.app')

@section('content')

    <h2>Категории продуктов</h2>

    @can('viewAny', Auth::User())
    <div>
        <a href="{{route('categories_of_products.create')}}" class="text-success">Добавить или отредактировать категорию</a>
    </div>
    @endcan

    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Название</th>
            </tr>
        </thead>
        <tbody>

            @php($categories_count = 0)
            @foreach($categories_of_products as $category_of_product)
                @php($categories_count = $categories_count + 1)
                <tr>
                    <th scope="row">{{$categories_count}}</th>
                    <td>{{$category_of_product->name}}</td>
                </tr>
            @endforeach

        </tbody>
    </table>

    <h2>Все продукты</h2>

    @can('viewAny', Auth::User())
    <div>
        <a href="{{route('products.create')}}" class="text-success">Добавить продукт</a>
    </div>
    @endcan

    <table class="table table-striped table-dark">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Название</th>
            <th scope="col">Цена</th>
            <th scope="col">Количество</th>
            <th scope="col">Категория</th>
        </tr>
        </thead>
        <tbody>
            @php($product_count = 0)
            @foreach($products as $product)
                @php($product_count = $product_count + 1)
            <tr>
                    <th scope="row">{{$product_count}}</th>
                    <td><a href="{{route('products.show', ['product' => $product])}}">{{$product->name}}</a></td>
                    <td>{{$product->price}}</td>
                    <td>{{$product->quantity}}</td>
                    <td>{{$product->category_of_products->name}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
