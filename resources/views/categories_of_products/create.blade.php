@extends('layouts.app')

@section('content')

    <form method="post" action="{{route('categories_of_products.store')}}">
        @csrf
        <div class="form-group">
            <label for="name">Название категории</label>
            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name">
            @error('name')
            <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>

        <button type="submit" class="btn btn-success">Создать</button>
    </form>

    <table class="table table-bordered">
        <tbody>
        @foreach($categories_of_products as $category_of_products)
            <tr>
                <td>{{$category_of_products->name}}</td>
                <td><a href="{{route('categories_of_products.edit', ['categories_of_product' => $category_of_products])}}">Редактировать</a></td>
                <td>
                    <form method="post" action="{{route('categories_of_products.destroy', ['categories_of_product' => $category_of_products])}}">
                        @method('delete')
                        @csrf
                        <button type="submit" class="btn btn-outline-danger">Удалить</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div class="mt-3 mb-3">
        <a href="{{route('index')}}">Назад</a>
    </div>

@endsection
