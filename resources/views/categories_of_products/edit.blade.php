@extends('layouts.app')

@section('content')

    <h3 class="mb-2 mt-2">{{$category_of_products->name}}</h3>

    <form method="post" action="{{route('categories_of_products.update', ['categories_of_product' => $category_of_products])}}">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="name">Название категории</label>
            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{$category_of_products->name}}">
            @error('name')
            <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>

        <button type="submit" class="btn btn-success">Редактировать</button>
    </form>

    <div class="mt-3 mb-3">
        <a href="{{route('categories_of_products.create')}}">Назад</a>
    </div>

@endsection
