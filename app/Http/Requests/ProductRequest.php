<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:256',
            'category_of_products_id' => 'required',
            'price' => 'required|numeric',
            'quantity' => 'numeric',
            'body' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Поле не должно быть пустым!',
            'name.min:3' => 'Минимум 3 символа',
            'name.max:256' => 'Максимум 256 символов',
            'price.required' => 'Поле не должно быть пустым',
            'price.numeric' => 'Введите число',
            'body.required' => 'Поле не должно быть пустым',
            'category_of_products_id.required' => 'Поле не должно быть пустым',
            'quantity.numeric' => 'Введите число'
        ];
    }
}
