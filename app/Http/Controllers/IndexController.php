<?php

namespace App\Http\Controllers;

use App\Models\CategoryOfProducts;
use App\Models\Product;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
    {
        $categories_of_products = CategoryOfProducts::all();
        $products = Product::all();

        return view('index', compact('categories_of_products', 'products'));
    }
}
