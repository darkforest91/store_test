<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\CategoryOfProducts;
use App\Models\Product;

class ProductController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        $categories_of_products = CategoryOfProducts::all();

        return view('products.create', compact('categories_of_products'));
    }

    /**
     * @param ProductRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ProductRequest $request)
    {
        $product = new Product($request->all());
        $product->save();

        return back()->with('status', 'Добавлен продукт ' . $product->name);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);

        return view('products.show', compact('product'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($id)
    {
        if ($this->checkForAdmin()) {
            $product = Product::findOrFail($id);
            $categories_of_products = CategoryOfProducts::all();

            return view('products.edit', compact('product', 'categories_of_products'));
        } else {
            return redirect('/');
        }

    }

    /**
     * @param ProductRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(ProductRequest $request, $id)
    {
        $product = Product::findOrFail($id);
        $this->authorize('update', $product);
        $product->update($request->all());

        return back()->with('status', 'Вы отредактировали продукт ' . $product->name);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $deleted_product = $product->name;
        $product->delete();

        return redirect('/')->with('status', 'Вы удалили ' . $deleted_product);
    }
}
