<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryOfProductsRequest;
use App\Models\CategoryOfProducts;
use Illuminate\Http\Request;

class CategoryOfProductsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        $categories_of_products = CategoryOfProducts::all();

        return view('categories_of_products.create', compact('categories_of_products'));
    }

    /**
     * @param CategoryOfProductsRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(CategoryOfProductsRequest $request)
    {
        $category_of_products = new CategoryOfProducts($request->all());
        $category_of_products->save();

        return redirect('categories_of_products/create')->with('status', 'Создана категория ' . $category_of_products->name);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $category_of_products = CategoryOfProducts::findOrFail($id);

        return view('categories_of_products.edit', compact('category_of_products'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request, $id)
    {
        $category_of_products = CategoryOfProducts::findOrFail($id);
        $this->authorize('update', $category_of_products);
        $category_of_products->update($request->all());

        return back()->with('status', 'Категория изменена на ' . $category_of_products->name);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $category_of_products = CategoryOfProducts::findOrFail($id);
        $category_of_products->delete();

        return back()->with('status', 'Категория удалена');
    }
}
