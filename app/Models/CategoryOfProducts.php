<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryOfProducts extends Model
{
    protected $fillable = [
        'name'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
