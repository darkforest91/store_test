<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'category_of_products_id',
        'price',
        'quantity',
        'body'
    ];

  /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category_of_products()
    {
        return $this->belongsTo(CategoryOfProducts::class);
    }
}
